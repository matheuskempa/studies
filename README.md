# Studies

## Description 
This repository will store all kind of my personal daily studies, since daily logical problems face it on my routine, until more complex problems. All the scripts will have a folder with an specific documentation for the solution of that problem.


## Folder's organization

- logical_problems: This folder will storage all sort of logical problems.


## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
