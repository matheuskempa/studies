def ordenar(lista_principal):
    """
    Order all the elements of a list.
    
    Params
    --------    
    lista: list
    
    Returns
    --------
    lista_final: list
        Updated list.
    """
    lista = lista_principal.copy()
    
    lista_inicial = [min(lista),max(lista)]
    
    qntd_inicial_minimo = lista.count(min(lista))
    
    qntd_inicial_maximo = lista.count(max(lista))
    
    lista  = list(filter(lambda a: a != min(lista) and a!= max(lista), lista))
    
    if qntd_inicial_minimo==1:
        pass
    else:
        lista.extend([min(lista_principal) for i in range(qntd_inicial_minimo-1)])
        
    if qntd_inicial_maximo==1:
        pass
    else:
        lista.extend([max(lista_principal) for i in range(qntd_inicial_maximo-1)])

    lista_final = lista_inicial.copy()
    for numero in lista:
        lista_final = _add_numero_nova_lista(numero,lista_final)

    return lista_final

def _add_numero_nova_lista(numero,lista):
    """
    Add the analyzed number to its respective position within the list.
    
    Params
    --------
    numero: int
    
    lista_geral: list
    
    Returns
    --------
    lista: list
        Updated list.
    """    

    for elemento in enumerate(lista):
        
        elemento_anterior = elemento[0]-1
        elemento_proximo = elemento[0]+1

        if len(lista) == 2:
            lista.insert(1,numero)
            break
        elif elemento_anterior < 0 :
            pass
        else:
            valor_atual = lista[elemento[0]]
            
            valor_proximo = lista[elemento_proximo]
            
            if numero < valor_atual:
                lista.insert(elemento[0],numero)
                break
            elif valor_proximo >= numero:
                lista.insert(elemento_proximo,numero)
                break
            else:
                pass
        
    return lista
