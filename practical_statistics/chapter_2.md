# Chapter 2 Distribuição de Dados e Amostras


## Distribuição de Amostragem de uma Estatística

Esse termo se refere à distribuição de alguma amostra estatística dentre muitas amostras extraídas de uma mesma população.

Como temos somente uma amostra de uma população, pode ser um erro, pois poderia ser diferente em outra amostra extraída. O ponto crucial aqui então é **variabilidade amostral**, ou seja, o quanto uma amostra pode ser diferente da outra.

A distriubuição de uma estatística amostral como a média costuma ser mais retangular e campanular do que a distribuição dos próprios dados. Quanto maior a amostra em que a estatística se baseia, mais isso é verdade. Além disso, quanto maior a amostra, mais estreita é a distribuição da estatística amostral.

## Teorema de Limite Central

Esse fenônemo informa que as médias extraídas de múltiplas amostras serão semelhantes à conhecida curva normal, mesmo se a população nõa for normalmente distribuída, já que o tamanho da amostra é grande o bastante e o desvio da normalidade não é muito grande. Esse teorema permite fórmulas de aproximação normal como a distribuição t.

## Erro padrão

O erro padrão é uma métrica única que resume a variabilidade na distribuição de amostragem de uma estatística. Ele é estimado utilizando uma estatística baseada no desvio padrão s dos valores da amostra e no tamanho de amostra n:



$$SE = {s\over \sqrt{n}}$$

SE = Standart error


> Não confundir desvio padrão com erro padrão.
> O desvio padrão mede a vairabilidade de pontos de dados individuais enquanto o erro padrão mede a variabilidade de da métrica de uma amostra.

Na prática coletar novas amostras para estimar o erro-padrão não costuma ser viável. Felizmente, acontece que não é necessário extrair novas amostras. Em ves disso pode-se usaro bootstrap. Na estatística moderna o bootstrap se tornou o modo padrão de estimar o erro padrão. Pode ser usado para praticamente qualquer estatística e não se baseia no teorema do limite central ou outras suposições distribuicionais. 

## Bootstrap

Fazer o processo de retirada de inúmeras amostras de uma população seria muito custoso, e por isso a abordagem do Boostrapping é sensacional, pois a partir de uma amostra nós retiramos dela diversas outras "amostras" e populamos um dataset "boostrapped". Após isso calculamos a média (ou qualquer outra medida) para aquele dataset e registramos essa informação, repetimos o procedimento inúmeras vezes e assim teremos um histograma, que nos permite ver como a medida escolhida( no nosso caso a média) variou ao longo de inúmeros experimentos. O ponto principal do bootstrap é que ele permite aplicarmos isso a qualquer estatística, criando um histograma e assim descobrindo o que aconteceria caso repetissemos aquele experimento inúmeras vezes.

![bootstrapping process.PNG](/practical_statistics/images/bootstrapping_process.PNG) 

